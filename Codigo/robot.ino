#include <Servo.h>
#include <SensorUltrasonidos.h>

/* PINS */
const int sensorTrigger = 11;
const int sensorEcho = 10;
const int servoBrazoIzquierdo = 8;
const int servoBrazoDerecho = 9;
const int servoCodoIzquierdo = 0;
const int servoCodoDerecho = 0;
const int motorDerechoPositivo = 2;
const int motorDerechoNegativo = 3;
const int potenciaMotorDerecho = 5;
const int motorIzquierdoPositivo = 7;
const int motorIzquierdoNegativo = 4;
const int potenciaMotorIzquierdo = 6;

/* CONFIG */
const int distanciaMinima = 15;

/* CONSTANTS */
const int BRAZOS_ADELANTE = 2000;
const int BRAZOS_QUIETOS = 1500;
const int BRAZOS_ATRAS = 1000;

SensorUltrasonidos sensor;
Servo brazoIzquierdo;
Servo brazoDerecho;
Servo codoIzquierdo;
Servo codoDerecho;

void setup() {
  sensor.set("trigger", sensorTrigger);
  sensor.set("echo", sensorEcho);
  brazoIzquierdo.attach(servoBrazoIzquierdo);
  brazoDerecho.attach(servoBrazoDerecho);
  codoIzquierdo.attach(servoCodoIzquierdo);
  codoDerecho.attach(servoCodoDerecho);

  pinMode(motorDerechoPositivo, OUTPUT);
  pinMode(motorDerechoNegativo, OUTPUT);
  pinMode(potenciaMotorDerecho, OUTPUT);
  pinMode(motorIzquierdoPositivo, OUTPUT);
  pinMode(motorIzquierdoNegativo, OUTPUT);
  pinMode(potenciaMotorIzquierdo, OUTPUT);

  Serial.begin(9600);
}

void loop () {
  avanzar();
  
  long distancia = sensor.getDistance();
  Serial.println("[SENSOR] Distancia: " + String(distancia)); 

  if(distancia < distanciaMinima) {
    parar();
    
    moverBrazos("adelante");
    moverCodos("doblar");
    delay(600);

    moverBrazos("atras");
    moverCodos("estirar");
    delay(430);
    
    moverBrazos("parar");
    girar();
    delay(4000);
  } else delay(1000);
}

void moverBrazos(const String& direccion) {
  Serial.println("[BRAZOS] Moviendo " + String(direccion)); 
  if(direccion == "atras") {
    brazoIzquierdo.writeMicroseconds(BRAZOS_ATRAS);
    brazoDerecho.writeMicroseconds(BRAZOS_ADELANTE);
  } else if(direccion == "adelante"){
    brazoIzquierdo.writeMicroseconds(BRAZOS_ADELANTE);
    brazoDerecho.writeMicroseconds(BRAZOS_ATRAS);
  } else {
    brazoIzquierdo.writeMicroseconds(BRAZOS_QUIETOS);
    brazoDerecho.writeMicroseconds(BRAZOS_QUIETOS);
  }
}


void moverCodos(const String& direccion) {
  Serial.println("[CODOS] Haciendo: " + String(direccion)); 
  if(direccion == "doblar") {
    codoIzquierdo.write(95);
    codoDerecho.write(95);
  } else if(direccion == "estirar"){
    codoIzquierdo.write(0);
    codoDerecho.write(0);
  }
}

void avanzar() { 
  Serial.println("[ORUGAS] Avanzando");
  digitalWrite (motorIzquierdoPositivo, HIGH);
  digitalWrite (motorIzquierdoNegativo, LOW);
  analogWrite (potenciaMotorIzquierdo, 255);

  digitalWrite (motorDerechoPositivo, HIGH);
  digitalWrite (motorDerechoNegativo, LOW);
  analogWrite (potenciaMotorDerecho, 250);
}

void parar() { 
  Serial.println("[ORUGAS] Parando");
  analogWrite (potenciaMotorIzquierdo, 0);
  analogWrite (potenciaMotorDerecho, 0);
}

void girar() {
  // int sentido = random(0, 2);
  int sentido = 1;
  Serial.println("[ORUGAS] Girando " + String(sentido));
  if(sentido == 1) {
     digitalWrite (motorIzquierdoPositivo, HIGH);
     digitalWrite (motorIzquierdoNegativo, LOW);
     analogWrite (potenciaMotorIzquierdo, 255);
     
     digitalWrite (motorDerechoPositivo, LOW);
     digitalWrite (motorDerechoNegativo, HIGH);
     analogWrite (potenciaMotorDerecho, 0);
  } else {
    
  }
}


